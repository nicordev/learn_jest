class TimerBlocking {
  elapsedSeconds: number;
  endCallback: CallableFunction;
  tickCallback: CallableFunction;

  constructor(endCallback: CallableFunction, tickCallback: CallableFunction) {
    this.endCallback = endCallback ?? (() => {});
    this.tickCallback = tickCallback ?? (() => {});
    this.elapsedSeconds = 0;
  }

  startBlocking(durationInSeconds: number) {
    const endAt = Date.now() + durationInSeconds * 1000;
    this.elapsedSeconds = 0;
    let previousTime = 0;

    do {
      this.elapsedSeconds =
        durationInSeconds - Math.floor((endAt - Date.now()) / 1000);
      if (this.elapsedSeconds > previousTime) {
        this.tickCallback(this.elapsedSeconds);
        previousTime = this.elapsedSeconds;
      }
    } while (this.elapsedSeconds < durationInSeconds);

    this.endCallback();
  }
}

export default TimerBlocking;

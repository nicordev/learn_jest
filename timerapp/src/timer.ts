class Timer {
  durationInSeconds: number;
  elapsedSeconds: number;
  timerInProgressId: any;
  endCallback: CallableFunction;
  tickCallback: CallableFunction;

  constructor(endCallback: CallableFunction|null, tickCallback: CallableFunction|null) {
    this.endCallback = endCallback ?? (() => {});
    this.tickCallback = tickCallback ?? (() => {});
    this.elapsedSeconds = 0;
    this.durationInSeconds = 0;
    this.timerInProgressId = null;
  }

  start(durationInSeconds: number) {
    this.durationInSeconds = durationInSeconds;
    this.elapsedSeconds = 0;
    this.#runTimer();
  }

  pause() {
    this.#stopTimer();
  }

  resume() {
    this.#runTimer();
  }

  reset() {
    this.#stopTimer();
    this.elapsedSeconds = 0;
  }

  #runTimer() {
    this.timerInProgressId = setInterval(() => {
      this.elapsedSeconds++;
      this.tickCallback(this.elapsedSeconds);
      if (this.elapsedSeconds >= this.durationInSeconds) {
        this.endCallback();
        this.reset();
      }
    }, 1000);
  }

  #stopTimer() {
    clearInterval(this.timerInProgressId);
  }
}

export default Timer;

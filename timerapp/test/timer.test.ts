import Timer from '../src/timer.ts'

function wait(ms: number) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}

test("timer execute callback on end", async () => {
  let isEndCallbackCalled = false;
  const endCallback = () => (isEndCallbackCalled = true);
  const timer = new Timer(endCallback, () => {});
  timer.start(1);
  setTimeout(() => {
    expect(isEndCallbackCalled).toBe(true);
  }, 1010);
  await wait(1020);
});

test("timer execute callback on each tic", async () => {
  let isTickCallbackCalled = false;
  const endCallback = () => {};
  const tickCallback = (elapsedSeconds: number) => {
    isTickCallbackCalled = true;
    console.log(elapsedSeconds + (elapsedSeconds % 2 === 1 ? " tic" : " tac"));
  };
  const timer = new Timer(endCallback, tickCallback);
  timer.start(3);
  setTimeout(() => {
    expect(isTickCallbackCalled).toBe(true);
  }, 3010);
  await wait(3020);
});

test("timer can be paused", async () => {
  let tics = 0;
  const endCallback = () => {};
  const tickCallback = (elapsedSeconds: number) => {
    tics++;
  };
  const timer = new Timer(endCallback, tickCallback);
  setTimeout(() => {
    timer.pause();
    expect(tics).toBe(1);
  }, 1010);
  setTimeout(() => {
    expect(tics).toBe(1);
    timer.resume();
  }, 2003);
  timer.start(3);
  await wait(3020);
  expect(tics).toBe(2);
});

test("timer can be reset", async () => {
  const timer = new Timer(null, null);
  timer.start(3);
  await wait(1020);
  timer.reset();
  expect(timer.elapsedSeconds).toBe(0);
});

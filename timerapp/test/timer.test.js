class TimerBlocking {
  elapsedSeconds;

  constructor(endCallback, tickCallback) {
    this.endCallback = endCallback ?? (() => {});
    this.tickCallback = tickCallback ?? (() => {});
    this.elapsedSeconds = 0;
  }

  start(durationInSeconds) {
    const endAt = Date.now() + durationInSeconds * 1000;
    this.elapsedSeconds = 0;
    let previousTime = 0;

    do {
      this.elapsedSeconds =
        durationInSeconds - Math.floor((endAt - Date.now()) / 1000);
      if (this.elapsedSeconds > previousTime) {
        this.tickCallback(this.elapsedSeconds);
        previousTime = this.elapsedSeconds;
      }
    } while (this.elapsedSeconds < durationInSeconds);

    this.endCallback();
  }
}

class Timer {
  durationInSeconds;
  elapsedSeconds;
  timerInProgressId;

  constructor(endCallback, tickCallback) {
    this.endCallback = endCallback ?? (() => {});
    this.tickCallback = tickCallback ?? (() => {});
    this.elapsedSeconds = 0;
    this.durationInSeconds = 0;
    this.timerInProgressId = null;
  }

  start(durationInSeconds) {
    this.durationInSeconds = durationInSeconds;
    this.elapsedSeconds = 0;
    this.#runTimer();
  }

  pause() {
    this.#stopTimer();
  }

  resume() {
    this.#runTimer();
  }

  reset() {
    this.#stopTimer();
    this.elapsedSeconds = 0;
  }

  #runTimer() {
    this.timerInProgressId = setInterval(() => {
      this.elapsedSeconds++;
      this.tickCallback(this.elapsedSeconds);

      if (this.elapsedSeconds >= this.durationInSeconds) {
        this.endCallback();
        this.reset();
      }
    }, 1000);
  }

  #stopTimer() {
    clearInterval(this.timerInProgressId);
  }
}

function wait(ms) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}

test("timer execute callback on end", async () => {
  let isEndCallbackCalled = false;
  const endCallback = () => (isEndCallbackCalled = true);
  const timer = new Timer(endCallback, () => {});
  timer.start(1);
  setTimeout(() => {
    expect(isEndCallbackCalled).toBe(true);
  }, 1010);
  await wait(1020);
});

test("timer execute callback on each tic", async () => {
  let isTickCallbackCalled = false;
  const endCallback = () => {};
  const tickCallback = (elapsedSeconds) => {
    isTickCallbackCalled = true;
    console.log(elapsedSeconds + (elapsedSeconds % 2 === 1 ? " tic" : " tac"));
  };
  const timer = new Timer(endCallback, tickCallback);
  timer.start(3);
  setTimeout(() => {
    expect(isTickCallbackCalled).toBe(true);
  }, 3010);
  await wait(3020);
});

test("timer can be paused", async () => {
  let tics = 0;
  const endCallback = () => {};
  const tickCallback = (elapsedSeconds) => {
    tics++;
  };
  const timer = new Timer(endCallback, tickCallback);
  setTimeout(() => {
    timer.pause();
    expect(tics).toBe(1);
  }, 1010);
  setTimeout(() => {
    expect(tics).toBe(1);
    timer.resume();
  }, 2003);
  timer.start(3);
  await wait(3020);
  expect(tics).toBe(2);
});

test("timer can be reset", async () => {
  const timer = new Timer();
  timer.start(3);
  await wait(1020);
  timer.reset();
  expect(timer.elapsedSeconds).toBe(0);
});

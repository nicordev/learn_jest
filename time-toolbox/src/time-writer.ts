class TimeWriter
{
    write(seconds: number): string {
        const hoursCount = Math.floor(seconds / 3600)
        const minutesCount = Math.floor((seconds - hoursCount * 3600) / 60)
        const secondsCount = (seconds % 60)

        return [
            hoursCount,
            minutesCount,
            secondsCount,
        ].map((part: number) => part.toFixed(0).padStart(2, '0')).join(':')
    }
}

export default TimeWriter

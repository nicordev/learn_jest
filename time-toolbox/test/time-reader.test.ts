import TimeReader from '../src/time-reader.ts'

test("can read a second", async () => {
    const timeReader = new TimeReader()
    expect(timeReader.read('1')).toBe(1)
});

test("can read 10 second", async () => {
    const timeReader = new TimeReader()
    expect(timeReader.read('10')).toBe(10)
});

test("can read 1 minute", async () => {
    const timeReader = new TimeReader()
    expect(timeReader.read('1:0')).toBe(60)
});

test("can read 1 hour", async () => {
    const timeReader = new TimeReader()
    expect(timeReader.read('1:0:0')).toBe(3600)
});

test("can read 03:06:09", async () => {
    const timeReader = new TimeReader()
    expect(timeReader.read('03:06:09')).toBe(11169)
});

test("can read 03.06.09", async () => {
    const timeReader = new TimeReader()
    expect(timeReader.read('03.06.09')).toBe(11169)
});


test("can read 03,06,09", async () => {
    const timeReader = new TimeReader()
    expect(timeReader.read('03,06,09')).toBe(11169)
});

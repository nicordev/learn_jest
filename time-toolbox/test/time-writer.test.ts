import TimeWriter from '../src/time-writer.ts'

test("can write a second", () => {
    const timeReader = new TimeWriter()
    expect(timeReader.write(1)).toBe('00:00:01')
});

test("can write 10 seconds", () => {
    const timeReader = new TimeWriter()
    expect(timeReader.write(10)).toBe('00:00:10')
});

test("can write 1 minute", () => {
    const timeReader = new TimeWriter()
    expect(timeReader.write(60)).toBe('00:01:00')
});

test("can write 1 hour", () => {
    const timeReader = new TimeWriter()
    expect(timeReader.write(3600)).toBe('01:00:00')
});

test("can write 3 hours 6 minutes 9 seconds", () => {
    const timeReader = new TimeWriter()
    expect(timeReader.write(11169)).toBe('03:06:09')
});

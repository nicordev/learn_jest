const greet = require('../src/greet.js')

test(
    'greet World',
    () => {
        expect(greet('World')).toBe('Hello World!')
    }
)

test(
    'greet Bob',
    () => {
        expect(greet('Bob')).toBe('Hello Bob!')
    }
)

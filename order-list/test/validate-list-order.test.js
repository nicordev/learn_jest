function validateListOrder(items, property, descendingOrder)
{
    if (descendingOrder) {
        items.forEach((item, index, wholeArray) => {
            if (index > 0 && wholeArray[index - 1][property] < item[property]) {
                throw new Error(`descending order: ${property} ${wholeArray[index - 1][property]} must be after ${item[property]}.`)
            }
        });

        return
    }

    items.forEach((item, index, wholeArray) => {
        if (index > 0 && wholeArray[index - 1][property] > item[property]) {
            throw new Error(`ascending order: ${property} ${wholeArray[index - 1][property]} must be after ${item[property]}.`)
        }
    });
}

test(
    'validate ordered list ascending',
    () => {
        const items = [
            {
                min: 0
            },
            {
                min: 100
            },
            {
                min: 300
            },
            {
                min: 200
            },
            {
                min: 5000
            },
        ]
        let actualError = null;
        try {
            validateListOrder(items, 'min', false)
        } catch ({message}) {
            actualError = message
        }

        expect(actualError).not.toBeNull()
        expect(actualError).toBe('ascending order: min 300 must be after 200.')
    }
)

test(
    'validate ordered list descending',
    () => {
        const items = [
            {
                min: 5000
            },
            {
                min: 200
            },
            {
                min: 300
            },
            {
                min: 100
            },
            {
                min: 0
            },
        ]
        let actualError = null;
        try {
            validateListOrder(items, 'min', true)
        } catch ({message}) {
            actualError = message
        }

        expect(actualError).not.toBeNull()
        expect(actualError).toBe('descending order: min 200 must be after 300.')
    }
)

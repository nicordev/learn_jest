function orderList(items, descendingOrder)
{
    if (descendingOrder) {
        return items.sort((a, b) => b.min - a.min)
    }

    return items.sort((a, b) => a.min - b.min)
}

test(
    'order list ascending',
    () => {
        const items = [
            {
                min: 0
            },
            {
                min: 100
            },
            {
                min: 300
            },
            {
                min: 200
            },
            {
                min: 5000
            },
        ]
        expect(orderList(items, false)).toEqual([
            {
                min: 0
            },
            {
                min: 100
            },
            {
                min: 200
            },
            {
                min: 300
            },
            {
                min: 5000
            },
        ])
    }
)

test(
    'order list descending',
    () => {
        const items = [
            {
                min: 0
            },
            {
                min: 100
            },
            {
                min: 300
            },
            {
                min: 200
            },
            {
                min: 5000
            },
        ]
        expect(orderList(items, true)).toEqual([
            {
                min: 5000
            },
            {
                min: 300
            },
            {
                min: 200
            },
            {
                min: 100
            },
            {
                min: 0
            },
        ])
    }
)

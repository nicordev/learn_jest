class WordPressApiReader
{
    host: string
    postsUri = '/wp-json/wp/v2/posts'

    constructor (host: string)
    {
        this.host = host
    }

    async fetchPosts(offset = 1, perPage = 10, fields = ['title', 'excerpt', 'content', 'author', 'slug']): Promise<Post[]> {
        const params = new URLSearchParams({
            offset: `${offset}`,
            perPage: `${perPage}`,
            _fields: ['title', 'excerpt', 'content', 'author'].join(',')
        });

        const response = await fetch(this.getPostsUrl(params.toString()))

        if (!response.ok) {
            console.error(await response.json())
            const message = `An error has occured: ${response.status}`;
            throw new Error(message);
        }

        const posts = await response.json()

        return posts.map((post: WordPressPost): Post => ({
            title: post.title.rendered,
            body: post.content.rendered,
            userId: post.author,
        }))
    }

    getPostsUrl(query: string|null) {
        return `${this.host}${this.postsUri}?${query ?? ''}`
    }
}

interface WordPressPost {
    title: {
        rendered: string
    };
    content: {
        rendered: string
    };
    excerpt: {
        rendered: string
    };
    author: string;
    slug: string;
}

export interface Post {
    title: string;
    body: string;
    userId: string;
}

export default WordPressApiReader

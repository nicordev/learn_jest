import { host } from '../env.ts'
import WordPressApiReader from '../src/WordPressApiReader.ts'

test(
    'can generate an url for posts',
    () => {
        const apiReader = new WordPressApiReader(host)
        const result = apiReader.getPostsUrl('page=2&perPage=3');

        expect(result).toBe(`${host}/wp-json/wp/v2/posts?page=2&perPage=3`)
    }
)

test(
    'fetch posts',
    async () => {
        const apiReader = new WordPressApiReader(host)
        const results = await apiReader.fetchPosts();

        expect(Array.isArray(results)).toBeTruthy()
        expect(results.length).toBeGreaterThan(0)
        expect(results[0]).toHaveProperty('userId')
        expect(results[0]).toHaveProperty('title')
        expect(results[0]).toHaveProperty('body')
    }
)

test(
    'fetch posts paginated',
    async () => {
        const apiReader = new WordPressApiReader(host)
        const results = await apiReader.fetchPosts(1, 1);

        expect(Array.isArray(results)).toBeTruthy()
        expect(results.length).toBe(1)
        expect(results[0]).toHaveProperty('userId')
        expect(results[0]).toHaveProperty('title')
        expect(results[0]).toHaveProperty('body')
    }
)

# Jest

> [documentation](https://jestjs.io/docs/getting-started)

## installation

```sh
yarn add --dev jest
```

```sh
pnpm add --save-dev jest
```

```sh
npm install --save-dev jest
```

## usage

Add a test file named like this:

```
testFileNameHere.test.js
```

for instance `greet.test.js`:

```js
const greet = require('../src/greet.js')

test(
    'greet World',
    () => {
        expect(greet('World')).toBe('Hello World!')
    }
)
```

Add in package.json

```json
{
  "scripts": {
    "test": "jest"
  }
}
```

Run

```sh
yarn test
```

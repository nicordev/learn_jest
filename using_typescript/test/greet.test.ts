import { greet } from '../src/greet.ts'

test(
    'greet World',
    () => {
        expect(greet('World')).toBe('Hello World!')
    }
)

test(
    'greet Bob',
    () => {
        expect(greet('Bob')).toBe('Hello Bob!')
    }
)

# using typescript

add packages and types

```sh
yarn add --dev jest ts-jest typescript @types/jest
```

create a configuration file

```sh
yarn ts-jest config:init
tsc --init
```

set `allowImportingTsExtensions` to `true` in typescript configuration file `tsconfig.json`

```json
{
    "allowImportingTsExtensions": true,
}
```
